<?php
require_once 'vendor/autoload.php';

ini_set('date.timezone', 'Asia/Tokyo');
define('MY_TITLE', 'LOGIN');

$smarty = new Smarty();

//使うテンプレートが入っているディレクトリを指定
$smarty->setTemplateDir('./templates/');
$smarty->assign('Login', 'Login Smarty');;

if (!empty($_POST)) {
    $email = $_POST['email'];
    $passwd = $_POST['passwd'];

    if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
        $error[] = '正しいメールアドレスを入力してください';
    }
    if (strlen($email) > 100) {
        $error[] = 'メールアドレスが長すぎます';
    }
    if (strlen($passwd) > 30 || strlen($passwd) < 8) {
        $error[] = 'パスワードは8文字以上30桁以内で入力してください';
    }

    if (empty($error)) {
        header('Location:index.php');
    }
}

$smarty->assign('error', $error);
//
$smarty->display('login.tpl');
