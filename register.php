<?php
require_once 'vendor/autoload.php';

ini_set('date.timezone', 'Asia/Tokyo');
define('MY_TITLE', 'REGISTER');

$smarty = new Smarty();

//使うテンプレートが入っているディレクトリを指定
 $smarty->setTemplateDir('./templates/');
$smarty->assign('Register', 'Register Smarty');;

if (!empty($_POST)) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $passwd = $_POST['passwd'];
    //各入力項目のバリデート
    if (strlen($name) > 100) {
        $error[] = '名前が長すぎます';
    }
    if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
        $error[] = '正しいメールアドレスを入力してください';
    }
    if (strlen($email) > 100) {
        $error[] = 'メールアドレスが長すぎます';
    }
    if (strlen($passwd) > 30 || strlen($passwd) < 8) {
        $error[] = 'パスワードは8文字以上30桁以内で入力してください';
    }

    if (empty($error)) {

        $pdo = new PDO('mysql:dbname=SmartyTest;host=mysql', 'root', 'root');
        $stmt = $pdo->prepare("select * from smartyTable where 'email' = :email");
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();
        $error = $stmt->fetch();


//        $stmt = $pdo->prepare("insert into smartyTable (name, email, password) value (:name, :email, :password)");
//        $stmt->bindParam(':name',$name, PDO::PARAM_STR);
//        $stmt->bindParam(':email',$email, PDO::PARAM_STR);
//        $stmt->bindParam(':password',password_hash($passwd, PASSWORD_DEFAULT), PDO::PARAM_STR);
//        $stmt->execute();
//
//        header('Location:index.php');
    }

}

$smarty->assign('error', $error);
//
$smarty->display('register.tpl');