<!DOCTYPE html>
<meta charset="utf-8">
<head>
    <title> {$page_title} </title>
    <link rel="stylesheet" type="text/css" href="../smarty.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
</head>