{*
  コメントアウト
*}
<body>
{include file='header.tpl' page_title={$smarty.const.MY_TITLE}}

<div class="uplight">
    <a href="login.php">Login</a>
    <a href="register.php">Register</a>
</div>
<div class="center">
    <p class="title">{$hello}</p>
    <p class="date">
        {if isset($yourname)}
            こんにちは{$yourname}さん<br>
        {/if}
    {$today->format('Y/m/d (D)')}{$smarty.now}
    </p>
</div>
</body>